//
//  ViewController.swift
//  PixlrAssignment
//
//  Created by Adli Ramli on 14/09/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var drawView: DrawView! 
    @IBOutlet weak var sliderView: UISlider!
    weak var delegate: SliderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sliderView.isEnabled = false
        sliderView.maximumValue = 100
    }

    @IBAction func drawTwoByTwo(_ sender: Any) {
        drawView.drawShape(squares: .twoByTwo)
        sliderView.isEnabled = false
    }
    
    @IBAction func drawThreeByThree(_ sender: Any) {
        drawView.drawShape(squares: .threeByThree)
        sliderView.isEnabled = false
    }
    
    @IBAction func populateImage(_ sender: Any) {
        drawView.populateImage()
        sliderView.value = 50
        sliderView.isEnabled = true
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        delegate?.sliderValueChanged(value: sliderView.value)
    }
}
