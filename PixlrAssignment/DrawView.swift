//
//  DrawView.swift
//  PixlrAssignment
//
//  Created by Adli Ramli on 17/09/2020.
//  Copyright © 2020 Adli Ramli. All rights reserved.
//

import MetalPetal
import UIKit

enum Squares {
    case twoByTwo
    case threeByThree
}

protocol SliderDelegate: class {
    func sliderValueChanged(value: Float)
}

class DrawView: UIView {
    var currentSquares: Squares?
    let padding: CGFloat = 50
    var imageView: MTIImageView?
    var currentOutputImage: MTIImage?
    var currentFilter: MTIVibranceFilter?
    
    override func draw(_ rect: CGRect) {
        //drawing code
        guard let currentSquares = currentSquares else {
            return
        }
        subviews.forEach({ $0.removeFromSuperview() })
        self.layer.sublayers?.removeAll()
        switch currentSquares {
        case .twoByTwo:
            draw2x2Squares()
        case .threeByThree:
            draw3x3Squares()
        }
        
        if let parent = getParentViewController() as? ViewController {
            parent.delegate = self
        }
    }
    
    func draw2x2Squares() {
        let strokeDistance: CGFloat = (bounds.size.width - padding * 3) / 2
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = bounds
        shapeLayer.fillColor = nil
        shapeLayer.lineWidth = 2.0
        shapeLayer.strokeColor = UIColor.black.cgColor

        let square1path = UIBezierPath(rect: CGRect(x: padding, y: padding, width: strokeDistance, height: strokeDistance))
        let square2path = UIBezierPath(rect: CGRect(x: strokeDistance + padding * 2, y: padding, width: strokeDistance, height: strokeDistance))
        let square3path = UIBezierPath(rect: CGRect(x: padding, y: strokeDistance + padding * 2, width: strokeDistance, height: strokeDistance))
        let square4path = UIBezierPath(rect: CGRect(x: strokeDistance + padding * 2, y: strokeDistance + padding * 2, width: strokeDistance, height: strokeDistance))
        
        let shapeLayerPath = UIBezierPath()
        shapeLayerPath.append(square1path)
        shapeLayerPath.append(square2path)
        shapeLayerPath.append(square3path)
        shapeLayerPath.append(square4path)

        shapeLayer.path = shapeLayerPath.cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func draw3x3Squares() {
        let strokeDistance: CGFloat = (bounds.size.width - padding * 4) / 3
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = bounds
        shapeLayer.fillColor = nil
        shapeLayer.lineWidth = 2.0
        shapeLayer.strokeColor = UIColor.black.cgColor
        
        let square1path = UIBezierPath(rect: CGRect(x: padding, y: padding, width: strokeDistance, height: strokeDistance))
        let square2path = UIBezierPath(rect: CGRect(x: strokeDistance + padding * 2, y: padding, width: strokeDistance, height: strokeDistance))
        let square3path = UIBezierPath(rect: CGRect(x: (strokeDistance * 2) + (padding * 3), y: padding, width: strokeDistance, height: strokeDistance))
        let square4path = UIBezierPath(rect: CGRect(x: padding, y: strokeDistance + padding * 2, width: strokeDistance, height: strokeDistance))
        let square5path = UIBezierPath(rect: CGRect(x: strokeDistance + padding * 2, y: strokeDistance + padding * 2, width: strokeDistance, height: strokeDistance))
        let square6path = UIBezierPath(rect: CGRect(x: (strokeDistance * 2) + (padding * 3), y: strokeDistance + padding * 2, width: strokeDistance, height: strokeDistance))
        let square7path = UIBezierPath(rect: CGRect(x: padding, y: (strokeDistance * 2) + (padding * 3), width: strokeDistance, height: strokeDistance))
        let square8path = UIBezierPath(rect: CGRect(x: strokeDistance + padding * 2, y: (strokeDistance * 2) + (padding * 3), width: strokeDistance, height: strokeDistance))
        let square9path = UIBezierPath(rect: CGRect(x: (strokeDistance * 2) + (padding * 3), y: (strokeDistance * 2) + (padding * 3), width: strokeDistance, height: strokeDistance))

        let shapeLayerPath = UIBezierPath()
        shapeLayerPath.append(square1path)
        shapeLayerPath.append(square2path)
        shapeLayerPath.append(square3path)
        shapeLayerPath.append(square4path)
        shapeLayerPath.append(square5path)
        shapeLayerPath.append(square6path)
        shapeLayerPath.append(square7path)
        shapeLayerPath.append(square8path)
        shapeLayerPath.append(square9path)
        
        shapeLayer.path = shapeLayerPath.cgPath
        
        self.layer.addSublayer(shapeLayer)
    }
    
    func populateImage() {
        guard let image = UIImage(named: "map") else { return }
        switch currentSquares {
        case .twoByTwo:
            let strokeDistance: CGFloat = (bounds.size.width - padding * 3) / 2
            let squareFrame = CGRect(x: padding, y: padding, width: strokeDistance, height: strokeDistance)
            guard let cgImage = image.cgImage else { return }
            createMTIImage(squareFrame: squareFrame, cgImage: cgImage)
            
        case .threeByThree:
            let strokeDistance: CGFloat = (bounds.size.width - padding * 4) / 3
            let squareFrame = CGRect(x: padding, y: padding, width: strokeDistance, height: strokeDistance)
            guard let cgImage = image.cgImage else { return }
            createMTIImage(squareFrame: squareFrame, cgImage: cgImage)
        case .none:
            break
        }
    }
    
    func createMTIImage(squareFrame: CGRect, cgImage: CGImage) {
        let imageView = MTIImageView(frame: squareFrame)
        let filter = MTIVibranceFilter()
        filter.amount = 50
//        let filter = MTICLAHEFilter()
//        filter.clipLimit = 50
        filter.inputImage = MTIImage(cgImage: cgImage)
        self.currentFilter = filter
        imageView.image = filter.outputImage
        self.currentOutputImage = filter.outputImage
        imageView.contentMode = .scaleAspectFill
        self.imageView = imageView
        addSubview(self.imageView!)
    }
    
    func drawShape(squares: Squares) {
        currentSquares = squares
        setNeedsDisplay()
    }
}

extension DrawView: SliderDelegate {
    func sliderValueChanged(value: Float) {
        print("value: \(value)")
        if let filter = currentFilter {
            filter.amount = value
            self.imageView?.image = filter.outputImage
        }
    }
}

extension UIResponder {
    func getParentViewController() -> UIViewController? {
        if self.next is UIViewController {
            return self.next as? UIViewController
        } else {
            if self.next != nil {
                return (self.next!).getParentViewController()
            }
            else { return nil }
        }
    }
}

